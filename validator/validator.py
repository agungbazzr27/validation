from flask import Flask, render_template, request, url_for, redirect , make_response,jsonify, session
from flask import Blueprint,send_from_directory, send_file
from datetime import datetime
import argparse
import os.path
import sys
import time
import re
from openpyxl.reader.excel import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill
from openpyxl.utils import column_index_from_string, get_column_letter
from openpyxl.worksheet.datavalidation import DataValidation
from openpyxl.formatting import Rule
from openpyxl.styles.differential import DifferentialStyle
import mysql.connector
import uuid
from datetime import datetime
from . import app
from validator.auth import login_required
from validator.libvalidator import NotBlankValidator,TypeValidator,LengthValidator,RegexValidator,EmailValidator,ChoiceValidator,DateTimeValidator,ExcelDateValidator,CountryValidator,ConditionalValidator
conn = mysql.connector.connect(
    user = app.config['MYSQL_USER'], 
    password=app.config['MYSQL_PASSWORD'],
    host=app.config['MYSQL_HOST'],
    database=app.config['MYSQL_DB'])


@app.route('/validate/')
def validate():
    if login_required():
        return redirect(url_for('login'))
    cursor = conn.cursor()
    sql = "SELECT * FROM template ORDER BY created_at DESC"
    cursor.execute(sql)
    template = cursor.fetchall()
    cursor.close()
    return render_template("validate.html",template=template)

@app.route('/upload/',methods=['GET', 'POST'])
def upload():
    session['start'] = datetime.now()
    if request.method == 'POST':
        excelFile =request.files['file']
        template =request.form['template']
        sheetname =request.form['sheetname']
        header =request.form['header']
        if excelFile.filename == '':
            error = 'Pilih File Excel'
            return make_response(jsonify(error=error))
        ext = excelFile.filename.split('.')[1]
        excelExt = ['xls','xlsx']
        
        fileNameEx = excelFile.filename
        if ext not in excelExt:
            error = 'File harus excel dengan format .xls atau .xlsx'
            return make_response(jsonify(error=error))
        wb = load_workbook(excelFile, data_only=True)
        id_template = template
        cursor = conn.cursor()
        sql = "SELECT s.*,m.message,m.config,m.`values` FROM setting s JOIN message m ON s.code = m.code WHERE s.id_template =%s ORDER BY s.columns ASC,s.code ASC"
        cursor.execute(sql,(id_template,))
        setting = cursor.fetchall()
        cursor = conn.cursor()
        sql = "SELECT * FROM template WHERE id_template =%s"
        cursor.execute(sql,(id_template,))
        gettemplate = cursor.fetchall()
        label = []
        double = []
        for a in gettemplate:
            b = a[1]+1
            for i in range(1,b):
                label.append((get_column_letter(i)))
        cursor.close()
        sensor = []
        tem = {}
        name = None
        for lab in label:
            for s in setting:
                if s[3] == lab:
                    if s[7] == 'number' or s[7] == 'string' :
                        sensor.append({s[8]:{'message':s[6],'type':s[7]}})
                    elif s[7] == 'min':
                        sensor.append({s[8]:{'minMessage':"%s : %s"%(s[6],s[5]),'min':int(s[5])}})
                    elif s[7] == 'max':
                        sensor.append({s[8]:{'maxMessage':"%s : %s"%(s[6],s[5]),'max':int(s[5])}})
                    elif s[8] == 'Double':
                        double.append(s[3])
                    elif s[5] == '-':
                        sensor.append({s[8]:{'message':s[6]}})
                    else:
                        sensor.append({s[8]:{'message':s[6],s[7]:s[5]}})
                    name = s[4]
            tem.update({lab:sensor})
            sensor = []
        settings = {'validators':tem, 'defaultValidator': None, 'excludes': [], 'range': None, 'header': header}
        print(settings)
        print(double)
        errors = []
        coordinate = []
        coordt= None
        data = []
        redFill = PatternFill(start_color='FFFF0000',
        end_color = 'FFFF0000',
        fill_type = 'solid')
        yellowFill = PatternFill(start_color='FFFFFF00',
        end_color = 'FFFF0000',
        fill_type = 'solid')
        greenFill = PatternFill(start_color='000FF000',
        end_color = 'FFFF0000',
        fill_type = 'solid')
        errors = []
        print(settings['range'])
        #open Excel file
        print("Parse Excel file")
        ws = wb.get_sheet_by_name(sheetname)
        print('Loading ...')
        if 'range' in settings and settings['range'] != None:
            settings['range'] = settings['range'] + (str)(ws.max_row)
        #iterate excel sheet
        rowCounter = 0
        for row in ws.iter_rows(settings['range']):
        
            columnCounter = 0
            rowCounter = rowCounter + 1
            #do not parse empty rows
            print('Loading ...{0}'.format(rowCounter) )
            if isEmpty(row):
                continue
            for cell in row:
                columnCounter = columnCounter + 1
                try:
                    value = cell.value
                except ValueError:
                    #case when it is not possible to read value at all from any reason
                    column = get_column_letter(columnCounter)
                    coordinates = "%s%d" % (column, rowCounter)
                    errors.append((coordinates, ValueError))

                #find header (first) row
                if settings['header'] != True:
                    if value == settings['header']:
                        settings['header'] = True
                    break

                #skip excludes column
                if  hasattr(cell, 'column') and cell.column in settings['excludes']:
                    continue

                column = get_column_letter(columnCounter)
                coordinates = "%s%d" % (column, rowCounter)
                coordt = coordinates
                if column in settings['validators']:
                    for type in settings['validators'][column]:
                        name = list(type.keys())[0]
                        if name != 'Conditional':
                            isValid(type, value, coordinates, errors)
                        else:
                            fieldB = list(type.values())[0]['fieldB']
                            value2 = ws[fieldB + str(rowCounter)].value
                            isValid(type, value, coordinates, errors, value2)

                elif settings['defaultValidator'] != None:
                    isValid(settings['defaultValidator'], value, coordinates, errors)

        print('Finish ...')
        printErrors = True
        tmpDir = app.config['PATH_FILE']

        print("Found %d error(s)" % len(errors))
        filename = "errors_" + time.strftime("%Y-%m-%d") + "_" + str(int(time.time()))+ "_" +sheetname + "_" + fileNameEx
        newFile = os.path.join(tmpDir , filename)
        crdt = int(rowCounter)
        crdt = crdt + 2
        cellA = ws['A'+str(crdt)]
        cellA.value = 'FileName'
        cellB = ws['B'+str(crdt)]
        cellB.value = fileNameEx
        crdt = crdt + 1
        cellA = ws['A'+str(crdt)]
        cellA.value = 'Jumlah data (Row, Column)'
        cellB = ws['B'+str(crdt)]
        cellB.value = coordt
        crdt = crdt + 1
        cellA = ws['A'+str(crdt)]
        cellA.value = 'Jumlah error'
        cellB = ws['B'+str(crdt)]
        cellB.value = len(errors)
        crdt = crdt + 1
        cellA = ws['A'+str(crdt)]
        cellA.value = 'Waktu scanning (Tanggal, Jam)'
        cellB = ws['B'+str(crdt)]
        cellB.value = "%s - %s" % (session['start'], datetime.now())
        crdt = crdt + 1
        cellA = ws['A'+str(crdt)]
        cellA.value = 'Template ID'
        cellB = ws['B'+str(crdt)]
        cellB.value = template
        crdt = crdt + 1
        cellA = ws['A'+str(crdt)]
        cellA.value = 'User Create'
        cellB = ws['B'+str(crdt)]
        cellB.value = session['username']
        cursor = conn.cursor()
        cursor.execute("INSERT INTO history(filename,jum_error,tanggal,jum_data,id_template,user_create,created_at) VALUES (%s,%s,%s,%s,%s,%s,%s)",(filename,len(errors),datetime.now(),coordt,template,session['username'],datetime.now(),))
        conn.commit()
        dxf = DifferentialStyle(fill=redFill)
        rule = Rule(type="duplicateValues", dxf=dxf, stopIfTrue=None)
        for col in double:
            ws.conditional_formatting.add('$%s%s:$%s%s'%(col,str(ws.min_row),col,str(ws.max_row)),rule)
        if (len(errors) > 0):
            for error in errors:
                cell = ws[error[0]]
                if printErrors:
                    value = str(cell.value)
                    cell.value = value +' #'+ ','.join(error[1])
                cell.fill = yellowFill
            #save error excel file
            print("[[Save file: " + newFile + "]]")
            try:
                wb.save(newFile)
            except Exception as e:
                print(e)
                exit(1)
            return make_response(jsonify(result=newFile,filename=filename))
        else:
            try:
                wb.save(newFile)
            except Exception as e:
                print(e)
                exit(1)
            return make_response(jsonify(result=newFile,filename=filename))
    else:
        error = 'Method tidak tersedia'
        return make_response(jsonify(error=error))
    #return send_file(files, as_attachment=True)
    #return render_template('validate.html',sheet=data,cor=er)



def isValid(settings, value, coordinate, errors, value2 = None):
    #validator list
    classmap = {
        'NotBlank': NotBlankValidator.NotBlankValidator,
        'Type': TypeValidator.TypeValidator,
        'Length': LengthValidator.LengthValidator,
        'Regex': RegexValidator.RegexValidator,
        'Email': EmailValidator.EmailValidator,
        'Choice': ChoiceValidator.ChoiceValidator,
        'Date': DateTimeValidator.DateTimeValidator,
        'ExcelDate': ExcelDateValidator.ExcelDateValidator,
        'Country': CountryValidator.CountryValidator,
        'Conditional': ConditionalValidator.ConditionalValidator
    }

    violations = []
    type = list(settings.keys())[0]
    data = list(settings.values())[0]
    validator = classmap[type](data)

    if type != 'Conditional':
        result = validator.validate(value)
    else:
        result = validator.validate(value, value2)

    if (result == False):
        violations.append(validator.getMessage())

    if len(violations) > 0:
        errors.append((coordinate, violations))

    return True

def isEmpty(row):

    for cell in row:
        if cell.value:
            return False

    return True

@app.route('/getSheet/',methods=['POST','GET'])
def getSheet():
    sheet = None
    if request.method == 'POST':
        excelFile =request.files['file']
        if excelFile.filename == '':
            error = 'Pilih File Excel'
            return make_response(jsonify(error=error))
        ext = excelFile.filename.split('.')[1]
        excelExt = ['xls','xlsx']
        
        fileNameEx = excelFile.filename
        if ext not in excelExt:
            error = 'File harus excel dengan format .xls atau .xlsx'
            return make_response(jsonify(error=error))
        wb = load_workbook(excelFile, keep_vba=True, data_only=True, read_only=True)
        sheet = wb.sheetnames
    return jsonify(result=sheet)

@app.route('/download/',methods=['POST','GET'])
def download():
    if request.method == 'POST':
        path = request.form['path']
    return send_file(path,as_attachment=True)
