from flask import Flask, render_template, request, url_for, redirect , make_response,jsonify
from flask import Blueprint,send_from_directory, send_file, session
import mysql.connector
import uuid
from datetime import datetime
from . import app
conn = mysql.connector.connect(
    user = app.config['MYSQL_USER'], 
    password=app.config['MYSQL_PASSWORD'],
    host=app.config['MYSQL_HOST'],
    database=app.config['MYSQL_DB'])

@app.route('/login/')
def login():
    return render_template("login/login.html")


@app.route('/logout/')
def logout():
    session.pop('app_login')
    session.pop('username')
    session.pop('name_user')
    session.pop('id')
    session.pop('email')
    return redirect(url_for('login'))


@app.route('/auth/', methods=['GET','POST'])
def auth():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        cursor = conn.cursor()
        sql = "SELECT * FROM user WHERE username =%s"
        cursor.execute(sql,(username,))
        user = cursor.fetchall()
        if len(user)>0:
            for a in user:
                id_user = a[0]
                name_user = a[1]
                passworddb = a[3]
                role = a[4]
                email = a[5]
            if password == passworddb:
                session['app_login'] = True
                session['id'] = id_user
                session['name_user'] = name_user
                session['username'] = username
                session['id_user'] = id_user
                session['role'] = role
                session['email'] = email
                return redirect(url_for('home'))
            else:
                return render_template("login/login.html",error='Password yang anda masukan salah',username=username)
        else:
            return render_template("login/login.html",error='Username atau Password yang anda masukan salah',username=username)
    else:
        return render_template("login/login.html",error='salah',username=username)

    return redirect(url_for('home'))


@app.route('/regist/')
def regist():
    return render_template("regist.html")

def login_required():
    if 'app_login' not in session:
        return True
    else:
        return False