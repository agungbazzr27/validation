from flask import Flask, render_template, request, url_for, redirect , make_response,jsonify
from flask import Blueprint,send_from_directory, send_file
from datetime import datetime
import argparse
import os.path
import sys
import time
import re
from openpyxl.reader.excel import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill
from openpyxl.utils import column_index_from_string, get_column_letter
from openpyxl.worksheet.datavalidation import DataValidation
import mysql.connector
import uuid
from datetime import datetime
from validator.auth import login_required
from . import app
conn = mysql.connector.connect(
    user = app.config['MYSQL_USER'], 
    password=app.config['MYSQL_PASSWORD'],
    host=app.config['MYSQL_HOST'],
    database=app.config['MYSQL_DB'])

@app.route('/')
@app.route('/home/')
def home():
    if login_required():
        return redirect(url_for('login'))
    return render_template("home.html")

@app.route('/template/')
def template():
    if login_required():
        return redirect(url_for('login'))
    cursor = conn.cursor()
    sql = "SELECT * FROM template ORDER BY created_at DESC"
    cursor.execute(sql)
    template = cursor.fetchall()
    cursor.close()
    return render_template("template.html",template=template)

@app.route('/template/add/',methods=['POST','GET'])
def template_add():
    if login_required():
        return redirect(url_for('login'))
    data = []
    labelA = None
    sheet = None
    sheetname = ''
    minRow = 1
    if request.method == 'POST':
        excelFile =request.files['upload']
        minRow = request.form['row']
        sheetname = request.form['sheetname']
        # if excelFile.filename == '':
        #     error = 'Pilih File Excel'
        #     return redirect(url_for('validate',error=error,**request.args))
        # ext = excelFile.filename.split('.')[1]
        # excelExt = ['xls','xlsx']
        # fileNameEx = excelFile.filename
        # if ext not in excelExt:
        #     error = 'File harus excel dengan format .xls atau .xlsx'
        #     return redirect(url_for('validate',error=error,**request.args))
        wb = load_workbook(excelFile, keep_vba=True, data_only=True, read_only=True)
        sheet = wb.sheetnames
        try:
            ws = wb.get_sheet_by_name(sheetname)
            for row in ws.iter_rows(min_row=int(minRow),max_row=int(minRow)):
                columnCounter = 0
                if isEmpty(row):
                    continue
                for cell in row:
                    columnCounter = columnCounter + 1
                    try:
                        value = cell.value
                        column = get_column_letter(columnCounter)
                        data.append((column,value))
                    except ValueError:
                        pass
        except ValueError:
            pass
        labelA = 'A'            
    cursor = conn.cursor()
    sql = "SELECT * FROM message ORDER BY code ASC"
    cursor.execute(sql)
    message = cursor.fetchall()
    cursor.close()
    return render_template("temp/t_add.html",message=message,label=data,labelA=labelA,sheets=sheet,sheetname=sheetname,rowHeader=minRow)

@app.route('/template/save/',methods=['GET', 'POST'])
def template_save():
    if login_required():
        return redirect(url_for('login'))
    if request.method == 'POST':
        column = request.form.getlist('column[]')
        labels = request.form.getlist('labels[]')
        template_name = request.form['tname']
        uuids = uuid.uuid1()
        id_template = uuids.hex
        now = datetime.now()
        print(request.form)
        colCount = 0
        for col in column:
            label = labels[colCount]
            validator =  request.form.getlist('validator%s[]'%col)
            vCount = 1
            for val in validator:
                inputs = "inputs%s%d"%(col,vCount)
                inputsVal = '-'
                if inputs in request.form:
                    inputsVal = request.form[inputs]
                vCount = vCount+1
                cursor = conn.cursor()
                cursor.execute("INSERT INTO setting(id_template,code,columns,labels,inputs) VALUES (%s,%s,%s,%s,%s)",(id_template,val,col,label,inputsVal,))
                conn.commit()
            colCount = colCount + 1
        cursor = conn.cursor()
        cursor.execute("INSERT INTO template(id_template,columns,created_at,template_name) VALUES (%s,%s,%s,%s)",(id_template,len(column),now,template_name,))
        conn.commit()
    return redirect(url_for('template'))

@app.route('/template/detail/<id>')
def detail(id):
    if login_required():
        return redirect(url_for('login'))
    cursor = conn.cursor()
    sql = "SELECT s.*,m.message,m.config FROM setting s JOIN message m ON s.code = m.code WHERE s.id_template =%s ORDER BY s.columns ASC"
    cursor.execute(sql,(id,))
    setting = cursor.fetchall()
    cursor = conn.cursor()
    sql = "SELECT * FROM template WHERE id_template =%s"
    cursor.execute(sql,(id,))
    template = cursor.fetchall()
    label = []
    for a in template:
        b = a[1]+1
        for i in range(1,b):
            label.append((get_column_letter(i)))
    cursor.close()
    sensor = []
    tem = []
    name = None
    for lab in label:
        for s in setting:
            if s[3] == lab:
                if s[5] == '-':
                    b = s[6]
                else:
                    b = "%s %s(%s)"%(s[6],s[7],s[5])
                sensor.append(b)
                name = s[4]
        tem.append((lab,name,sensor))
        sensor = []
    return render_template("detail.html",template=template,id_template=id,setting=tem)

@app.route('/get_detail_template/',methods=['GET','POST'])
def getDetailTemplate():
    if login_required():
        return redirect(url_for('login'))
    if request.method == 'POST':
        id_template =request.form['id_template']
        cursor = conn.cursor()
        sql = "SELECT s.*,m.message,m.config,m.`values` FROM setting s JOIN message m ON s.code = m.code WHERE s.id_template =%s ORDER BY s.columns ASC,s.code"
        cursor.execute(sql,(id_template,))
        setting = cursor.fetchall()
        cursor = conn.cursor()
        sql = "SELECT * FROM template WHERE id_template =%s"
        cursor.execute(sql,(id_template,))
        template = cursor.fetchall()
        label = []
        for a in template:
            b = a[1]+1
            for i in range(1,b):
                label.append((get_column_letter(i)))
        cursor.close()
        sensor = []
        tem = []
        name = None
        for lab in label:
            for s in setting:
                if s[3] == lab:
                    if s[5] == '-':
                        b = s[6]
                    else:
                        b = "%s %s(%s)"%(s[6],s[7],s[5])
                    sensor.append(b)
                    name = s[4]
            tem.append((lab,name,sensor))
            sensor = []
        return jsonify(result=tem)
    else:
        id_template = '389804b390f311ea81c46036dd4fe312'
        cursor = conn.cursor()
        sql = "SELECT s.*,m.message,m.config,m.`values` FROM setting s JOIN message m ON s.code = m.code WHERE s.id_template =%s ORDER BY s.columns ASC,s.code ASC"
        cursor.execute(sql,(id_template,))
        setting = cursor.fetchall()
        cursor = conn.cursor()
        sql = "SELECT * FROM template WHERE id_template =%s"
        cursor.execute(sql,(id_template,))
        gettemplate = cursor.fetchall()
        label = []
        for a in gettemplate:
            b = a[1]+1
            for i in range(1,b):
                label.append((get_column_letter(i)))
        cursor.close()
        sensor = []
        tem = {}
        name = None
        for lab in label:
            for s in setting:
                if s[3] == lab:
                    if s[7] == 'number' or s[7] == 'string' :
                        sensor.append({s[8]:{'message':s[6],'type':s[7]}})
                    elif s[7] == 'min':
                        sensor.append({s[8]:{'minMessage':"%s : %s"%(s[6],s[5]),'min':int(s[5])}})
                    elif s[7] == 'max':
                        sensor.append({s[8]:{'maxMessage':"%s : %s"%(s[6],s[5]),'max':int(s[5])}})
                    elif s[5] == '-':
                        sensor.append({s[8]:{'message':s[6]}})
                    else:
                        sensor.append({s[8]:{'message':s[6],s[7]:s[5]}})
                    name = s[4]
            tem.update({lab:sensor})
            sensor = []
        settings = {'validators':tem, 'defaultValidator': {'NotBlank': {'message': 'Value can not be blank'}}, 'excludes': [], 'range': None, 'header': 'Nama'}
        settings = {'validators': {'A': [{'NotBlank': {'message': 'Value can not be blank'}}], 'B': [{'NotBlank': {'message': 'Value can not be blank'}},{'Length': {'max': 10, 'maxMessage': 'Value is too long'}}, {'Length': {'min': 1, 'maxMessage': 'Value is too short'}}], 'C': [{'Length': {'min': 3, 'minMessage': 'Value is too short'}}], 'D': [{'Length': {'min': 3, 'minMessage': 'Value is too short'}}]}, 'defaultValidator': {'NotBlank': {'message': 'Value can not be blank'}}, 'excludes': [12], 'range': None, 'header': 'Nama'}
        return jsonify(settings)
@app.route('/sensor/',methods=['GET', 'POST'])
def sensor():
    if login_required():
        return redirect(url_for('login'))
    if request.method == 'POST':
        code = request.form['code']
        message = request.form['message']
        values = request.form['values']
        cursor = conn.cursor()
        cursor.execute("INSERT IGNORE INTO message(message,code,`values`) VALUES (%s,%s,%s)",(message,code,values,))
        conn.commit()
        cursor = conn.cursor()
        sql = "SELECT * FROM message ORDER BY code ASC"
        cursor.execute(sql)
        message = cursor.fetchall()
        cursor.close()
    else:
        cursor = conn.cursor()
        sql = "SELECT * FROM message ORDER BY code ASC"
        cursor.execute(sql)
        message = cursor.fetchall()
        cursor.close()
    return render_template('m_add.html',message=message)
   
@app.route('/sensor/delete/<id>')
def sensor_delete(id):
    if login_required():
        return redirect(url_for('login'))
    cursor = conn.cursor()
    sql = "DELETE FROM `message` WHERE code =%s"
    cursor.execute(sql,(id,))
    cursor.close()
    return redirect(url_for('sensor'))

@app.route('/column/')
def column():
    if login_required():
        return redirect(url_for('login'))
    cursor = conn.cursor()
    sql = "SELECT * FROM message ORDER BY code ASC"
    cursor.execute(sql)
    message = cursor.fetchall()
    cursor.close()
    return render_template('c_add.html',message=message)

@app.route('/column2/')
def column2():
    if login_required():
        return redirect(url_for('login'))
    cursor = conn.cursor()
    sql = "SELECT * FROM message ORDER BY code ASC"
    cursor.execute(sql)
    message = cursor.fetchall()
    cursor.close()
    return render_template('c_add2.html',message=message)
    
def isEmpty(row):
    for cell in row:
        if cell.value:
            return False
    return True

@app.route('/getConfig/',methods=['POST','GET'])
def getConfig():
    if login_required():
        return jsonify(result="Access is disable")

    code = request.form['code']
    cursor = conn.cursor()
    sql = "SELECT * FROM `message` WHERE `code` = %s"
    cursor.execute(sql,(code,))
    message = cursor.fetchall()
    cursor.close()
    return jsonify(result=message)