from . import app    # For application discovery by the 'flask' command.
from . import auth    # For application discovery by the 'flask' command.
from . import views  # For import side-effects of setting up routes.
from . import validator  # For import side-effects of setting up routes.