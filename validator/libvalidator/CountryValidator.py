from . import BaseValidator
import pycountry

class CountryValidator(BaseValidator.BaseValidator):

    message = "This value is not correct country name"
    countries = pycountry.countries

    def validate(self, value):
        data_kota = ['Banda Aceh',
                    'Langsa',
                    'Lhokseumawe',
                    'Sabang',
                    'Subulussalam','Denpasar','Cilegon',
                    'Serang',
                    'Tangerang',
                    'Tangerang selatan','Bengkulu',
                    'Yogyakarta',
                    'Jakarta',
                    'DKI Jakarta',
                    'Jakarta Barat',
                    'Jakarta Pusat',
                    'Jakarta Selatan',
                    'Jakarta Timur',
                    'Jakarta Utara','Gorontalo','Jambi',
                    'Sungai Penuh','Bandung',
                    'Banjar',
                    'Bekasi',
                    'Bogor',
                    'Cimahi',
                    'Cirebon',
                    'Depok',
                    'Sukabumi',
                    'Tasikmalaya','Magelang',
                    'Pekalongan',
                    'Salatiga',
                    'Semarang',
                    'Surakarta',
                    'Tegal','Batu',
                    'Blitar',
                    'Kediri',
                    'Madiun',
                    'Malang',
                    'Mojokerto',
                    'Pasuruan',
                    'Probolinggo',
                    'Surabaya','Pontianak',
                    'Singkawang','Banjarbaru',
                    'Banjarmasin','Palangka Raya','Balikpapan',
                    'Bontang',
                    'Samarinda','Tarakan','Pangkal Pinang','Batam',
                    'Tanjung Pinang','Bandar Lampung',
                    'Kotabumi',
                    'Liwa',
                    'Metro','Ambon',
                    'Tual','Ternate',
                    'Tidore Kepulauan','Bima',
                    'Mataram','Kupang','Jayapura','Sorong','Dumai',
                    'Pekanbaru','Palopo',
                    'Parepare','Palu','Bau-Bau',
                    'Kendari','Bitung',
                    'Kotamobagu',
                    'Manado',
                    'Tomohon','Bukittinggi',
                    'Padang',
                    'Padangpanjang',
                    'Pariaman',
                    'Payakumbuh',
                    'Sawahlunto',
                    'Solok', 'Lubuklinggau',
                    'Pagar Alam',
                    'Palembang',
                    'Prabumulih','Binjai',
                    'Gunungsitoli',
                    'Medan',
                    'Padangsidempuan',
                    'Pematangsiantar',
                    'Sibolga',
                    'Tanjungbalai',
                    'Tebing Tinggi',]
        #possible null values
        if value is None:
            return True
        if value in data_kota:
            return True
        else:
            return False
        # value = super(CountryValidator, self).validate(value)
        # try:
        #     CountryValidator.countries.get(name = value)
        #     return True
        # except(KeyError):
        #     return False

    def __init__(self, params):
        super(CountryValidator, self).__init__(params)
