/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : validate_db

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-05-08 17:01:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for detail
-- ----------------------------
DROP TABLE IF EXISTS `detail`;
CREATE TABLE `detail` (
  `id_detail` int(10) NOT NULL AUTO_INCREMENT,
  `code` int(5) DEFAULT NULL,
  `config_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail
-- ----------------------------

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id_list` int(50) NOT NULL AUTO_INCREMENT,
  `filename` varchar(100) DEFAULT NULL,
  `jum_error` int(10) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jum_data` varchar(10) DEFAULT NULL,
  `id_template` varchar(50) DEFAULT NULL,
  `user_create` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_list`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of history
-- ----------------------------
INSERT INTO `history` VALUES ('1', 'errors_2020-04-25_1587759946_Data2.xlsx', '45', '2020-04-25 03:25:46', 'I10', 'c272437e84a111ea89316036dd4fe312', 'admin', '2020-04-25 03:25:46');
INSERT INTO `history` VALUES ('2', 'errors_2020-04-25_1587760479_Data2.xlsx', '3300', '2020-04-25 03:34:39', 'I1651', 'c272437e84a111ea89316036dd4fe312', 'admin', '2020-04-25 03:34:39');
INSERT INTO `history` VALUES ('3', 'errors_2020-04-27_1587963830_Data2.xlsx', '0', '2020-04-27 12:03:50', null, 'c272437e84a111ea89316036dd4fe312', 'admin', '2020-04-27 12:03:50');
INSERT INTO `history` VALUES ('4', 'errors_2020-05-01_1588312223_Data2.xlsx', '0', '2020-05-01 12:50:23', null, '7f6e8365847911ea95a66036dd4fe312', 'admin', '2020-05-01 12:50:23');
INSERT INTO `history` VALUES ('5', 'errors_2020-05-01_1588312533_Data2.xlsx_Sheet_Sheet1', '45', '2020-05-01 12:55:33', 'I10', '7f6e8365847911ea95a66036dd4fe312', 'admin', '2020-05-01 12:55:33');
INSERT INTO `history` VALUES ('6', 'errors_2020-05-01_1588312612_Sheet1_Data2.xlsx', '45', '2020-05-01 12:56:52', 'I10', 'c272437e84a111ea89316036dd4fe312', 'admin', '2020-05-01 12:56:52');
INSERT INTO `history` VALUES ('7', 'errors_2020-05-06_1588738398_PemetaanSensor_Sensors v2_3(1).xlsx', '0', '2020-05-06 11:13:18', null, 'c272437e84a111ea89316036dd4fe312', 'admin', '2020-05-06 11:13:18');
INSERT INTO `history` VALUES ('8', 'errors_2020-05-06_1588738497_PemetaanSensor_Sensors v2_3(1).xlsx', '0', '2020-05-06 11:14:57', null, '7f6e8365847911ea95a66036dd4fe312', 'admin', '2020-05-06 11:14:57');
INSERT INTO `history` VALUES ('9', 'errors_2020-05-07_1588855466_Sheet1_Data2.xlsx', '45', '2020-05-07 19:44:26', 'I10', 'c272437e84a111ea89316036dd4fe312', 'admin', '2020-05-07 19:44:26');
INSERT INTO `history` VALUES ('10', 'errors_2020-05-08_1588906826_Sheet1_Data2.xlsx', '0', '2020-05-08 10:00:26', null, 'd156a9dd90cc11eaa60d6036dd4fe312', 'admin', '2020-05-08 10:00:26');
INSERT INTO `history` VALUES ('11', 'errors_2020-05-08_1588907183_Sheet3_Data2.xlsx', '0', '2020-05-08 10:06:23', null, 'd333783090d811ea86316036dd4fe312', 'admin', '2020-05-08 10:06:23');
INSERT INTO `history` VALUES ('12', 'errors_2020-05-08_1588907196_Sheet4_Data2.xlsx', '0', '2020-05-08 10:06:36', null, 'd333783090d811ea86316036dd4fe312', 'admin', '2020-05-08 10:06:36');
INSERT INTO `history` VALUES ('13', 'errors_2020-05-08_1588907865_Sheet4_Data2.xlsx', '4', '2020-05-08 10:17:45', 'A12', 'd333783090d811ea86316036dd4fe312', 'admin', '2020-05-08 10:17:45');
INSERT INTO `history` VALUES ('14', 'errors_2020-05-08_1588907877_Sheet4_Data2.xlsx', '4', '2020-05-08 10:17:57', 'A12', 'd333783090d811ea86316036dd4fe312', 'admin', '2020-05-08 10:17:57');
INSERT INTO `history` VALUES ('15', 'errors_2020-05-08_1588908128_Sheet4_Data2.xlsx', '7', '2020-05-08 10:22:08', 'A12', 'fcfafdc990da11ea9eb96036dd4fe312', 'admin', '2020-05-08 10:22:08');
INSERT INTO `history` VALUES ('16', 'errors_2020-05-08_1588908430_Sheet4_Data2.xlsx', '7', '2020-05-08 10:27:10', 'A12', 'fcfafdc990da11ea9eb96036dd4fe312', 'admin', '2020-05-08 10:27:10');
INSERT INTO `history` VALUES ('17', 'errors_2020-05-08_1588908726_Sheet4_Data2.xlsx', '0', '2020-05-08 10:32:06', 'A12', 'fcfafdc990da11ea9eb96036dd4fe312', 'admin', '2020-05-08 10:32:06');
INSERT INTO `history` VALUES ('18', 'errors_2020-05-08_1588911036_Sheet4_Data2.xlsx', '10', '2020-05-08 11:10:36', 'A12', 'd0fb8c5490e111ea81626036dd4fe312', 'admin', '2020-05-08 11:10:36');
INSERT INTO `history` VALUES ('19', 'errors_2020-05-08_1588911129_Sheet4_Data2.xlsx', '0', '2020-05-08 11:12:09', 'A12', '086c008f90e211ea96636036dd4fe312', 'admin', '2020-05-08 11:12:09');
INSERT INTO `history` VALUES ('20', 'errors_2020-05-08_1588911380_Sheet4_Data2.xlsx', '0', '2020-05-08 11:16:20', 'A18', '086c008f90e211ea96636036dd4fe312', 'admin', '2020-05-08 11:16:20');
INSERT INTO `history` VALUES ('21', 'errors_2020-05-08_1588912029_Sheet4_Data2.xlsx', '7', '2020-05-08 11:27:09', 'A18', '2243b26f90e411ea9c886036dd4fe312', 'admin', '2020-05-08 11:27:09');
INSERT INTO `history` VALUES ('22', 'errors_2020-05-08_1588912236_Sheet4_Data2.xlsx', '4', '2020-05-08 11:30:36', 'A18', '086c008f90e211ea96636036dd4fe312', 'admin', '2020-05-08 11:30:36');
INSERT INTO `history` VALUES ('23', 'errors_2020-05-08_1588912648_Sheet1_Data2.xlsx', '0', '2020-05-08 11:37:28', null, '086c008f90e211ea96636036dd4fe312', 'admin', '2020-05-08 11:37:28');
INSERT INTO `history` VALUES ('24', 'errors_2020-05-08_1588912745_Sheet4_Data2.xlsx', '4', '2020-05-08 11:39:05', 'A18', '2243b26f90e411ea9c886036dd4fe312', 'admin', '2020-05-08 11:39:05');
INSERT INTO `history` VALUES ('25', 'errors_2020-05-08_1588912783_Sheet1_Data2.xlsx', '0', '2020-05-08 11:39:43', null, '2243b26f90e411ea9c886036dd4fe312', 'admin', '2020-05-08 11:39:43');
INSERT INTO `history` VALUES ('26', 'errors_2020-05-08_1588913296_Sheet4_Data2.xlsx', '26', '2020-05-08 11:48:16', 'A18', 'fc0139f690e611eab34a6036dd4fe312', 'admin', '2020-05-08 11:48:16');
INSERT INTO `history` VALUES ('27', 'errors_2020-05-08_1588913618_Sheet1_Data2.xlsx', '44', '2020-05-08 11:53:38', 'I10', 'fc0139f690e611eab34a6036dd4fe312', 'admin', '2020-05-08 11:53:38');
INSERT INTO `history` VALUES ('28', 'errors_2020-05-08_1588913988_Sheet4_Data2.xlsx', '26', '2020-05-08 11:59:48', 'A18', 'fc0139f690e611eab34a6036dd4fe312', 'admin', '2020-05-08 11:59:48');
INSERT INTO `history` VALUES ('29', 'errors_2020-05-08_1588914350_Sheet1_Data2.xlsx', '21', '2020-05-08 12:05:50', 'H10', '841652b290e911eabdaf6036dd4fe312', 'admin', '2020-05-08 12:05:50');
INSERT INTO `history` VALUES ('30', 'errors_2020-05-08_1588914833_Sheet1_Data2.xlsx', '20', '2020-05-08 12:13:53', 'H10', '841652b290e911eabdaf6036dd4fe312', 'admin', '2020-05-08 12:13:53');
INSERT INTO `history` VALUES ('31', 'errors_2020-05-08_1588914903_Sheet4_Data2.xlsx', '20', '2020-05-08 12:15:03', 'A18', '841652b290e911eabdaf6036dd4fe312', 'admin', '2020-05-08 12:15:03');
INSERT INTO `history` VALUES ('32', 'errors_2020-05-08_1588920011_Sheet1_Data2.xlsx', '27', '2020-05-08 13:40:11', 'H10', 'ace25a9490f611ea830c6036dd4fe312', 'admin', '2020-05-08 13:40:11');
INSERT INTO `history` VALUES ('33', 'errors_2020-05-08_1588921777_Sheet1_Data2.xlsx', '23', '2020-05-08 14:09:37', 'H10', 'aa10430c90fa11ea85366036dd4fe312', 'admin', '2020-05-08 14:09:37');
INSERT INTO `history` VALUES ('34', 'errors_2020-05-08_1588922848_Sheet1_Data2.xlsx', '32', '2020-05-08 14:27:28', 'H10', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:27:28');
INSERT INTO `history` VALUES ('35', 'errors_2020-05-08_1588923065_Sheet1_Data2.xlsx', '32', '2020-05-08 14:31:05', 'H10', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:31:05');
INSERT INTO `history` VALUES ('36', 'errors_2020-05-08_1588923620_Sheet1_Data2.xlsx', '35', '2020-05-08 14:40:20', 'H10', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:40:20');
INSERT INTO `history` VALUES ('37', 'errors_2020-05-08_1588923657_Sheet1_Data2.xlsx', '32', '2020-05-08 14:40:57', 'H9', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:40:57');
INSERT INTO `history` VALUES ('38', 'errors_2020-05-08_1588923908_Sheet1_Data2.xlsx', '0', '2020-05-08 14:45:08', null, '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:45:08');
INSERT INTO `history` VALUES ('39', 'errors_2020-05-08_1588924108_Sheet1_Data2.xlsx', '30', '2020-05-08 14:48:28', 'H8', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:48:28');
INSERT INTO `history` VALUES ('40', 'errors_2020-05-08_1588924439_Sheet1_Data2.xlsx', '30', '2020-05-08 14:53:59', 'H8', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:53:59');
INSERT INTO `history` VALUES ('41', 'errors_2020-05-08_1588924754_Sheet1_Data2.xlsx', '32', '2020-05-08 14:59:14', 'H10', '39f9521290fc11ea9d9f6036dd4fe312', 'admin', '2020-05-08 14:59:14');
INSERT INTO `history` VALUES ('42', 'errors_2020-05-08_1588931739_Sheet1_Data2.xlsx', '24', '2020-05-08 16:55:39', 'H10', 'd101d15c90f811eaa4806036dd4fe312', 'admin', '2020-05-08 16:55:39');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `code` int(5) NOT NULL,
  `message` varchar(200) DEFAULT NULL,
  `values` varchar(50) DEFAULT NULL,
  `config` varchar(30) NOT NULL,
  `input` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`code`,`config`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('100', 'Tidak Boleh kosong', 'NotBlank', '', '0');
INSERT INTO `message` VALUES ('101', 'Data Tidak Boleh Ganda', 'Double', '', '0');
INSERT INTO `message` VALUES ('300', 'Harus Berupa Angka', 'Type', 'number', '0');
INSERT INTO `message` VALUES ('400', 'Harus Berupa Huruf', 'Type', 'string', '0');
INSERT INTO `message` VALUES ('800', 'Panjang Maksimal', 'Length', 'max', '1');
INSERT INTO `message` VALUES ('801', 'Panjang Minimal', 'Length', 'min', '1');
INSERT INTO `message` VALUES ('1000', 'Menggunakan Regex', 'Regex', 'pattern', '1');

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id_setting` int(10) NOT NULL AUTO_INCREMENT,
  `id_template` varchar(50) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  `columns` varchar(5) DEFAULT NULL,
  `labels` varchar(200) DEFAULT NULL,
  `inputs` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setting
-- ----------------------------

-- ----------------------------
-- Table structure for template
-- ----------------------------
DROP TABLE IF EXISTS `template`;
CREATE TABLE `template` (
  `id_template` varchar(50) NOT NULL,
  `columns` int(5) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `template_name` varchar(255) DEFAULT NULL,
  `row_header` int(5) DEFAULT NULL,
  PRIMARY KEY (`id_template`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of template
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` varchar(50) NOT NULL,
  `name_user` varchar(34) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'ADMIN', 'admin', 'admin', '1', 'admin@sensor.com');
